const express = require("express");
const userRouter = express.Router();
const userController = require("./controller");

userRouter.get("/", userController.getAllUsers);
userRouter.post("/regis", userController.registerNewUser);
userRouter.post("/login", userController.loginUser);

module.exports = userRouter;
