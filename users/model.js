const md5 = require("md5");

let userList = [];

class UserModel {
  // cek user data
  getAllUsers = () => {
    return userList;
  };

  //  cek user existence
  isUserExist = (userData) => {
    const alrdExist = userList.find((data) => {
      return (
        data.username === userData.username || data.email === userData.email
      );
    });

    if (alrdExist) {
      return true;
    } else {
      return false;
    }
  };

  //    method register
  registerNewUser = (userData) => {
    userList.push({
      id: userList.length + 1,
      username: userData.username,
      email: userData.email,
      password: md5(userData.password),
    });
  };

  // method login
  userLogin = (username, email, password) => {
    const sortedUser = userList.find((user) => {
      return (
        (user.username === username || user.email === email) &&
        user.password === md5(password)
      );
    });

    return sortedUser;
  };
}

module.exports = new UserModel();
