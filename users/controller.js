const userModels = require("./model");

class UserController {
  getAllUsers = (req, res) => {
    const allUsers = userModels.getAllUsers();
    return res.json(allUsers);
  };

  registerNewUser = (req, res) => {
    const userData = req.body;

    if (
      userData.username == "" &&
      userData.password == "" &&
      userData.email == ""
    ) {
      return res.json({ message: "Tolong isi data diri dengan lengkap" });
    } else if (userData.password == "") {
      return res.json({ message: "Tolong masukan password kamu" });
    } else if (userData.email == "") {
      return res.json({ message: "Tolong masukan email kamu" });
    } else if (userData.username == "") {
      return res.json({ message: "Tolong masukan username kamu" });
    }

    const alrdExist = userModels.isUserExist(userData);

    if (alrdExist) {
      return res.json({ message: "Username or Email already exist" });
    }

    userModels.registerNewUser(userData);
    return res.json({ message: "Sukses menambahkan user baru!" });
  };

  loginUser = () => {
    const { username, email, password } = req.body;
    const loginData = userModels.userLogin(username, email, password);

    if (loginData) {
      return res.json(loginData);
    } else {
      res.statusCode = 404;
      return res.json({ message: "Credential Tidak ditemukan" });
    }
  };
}

module.exports = new UserController();
