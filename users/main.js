const express = require("express");
const app = express();
const userRouter = require("./route");

app.use(express.json());

app.listen(3000, () => {
  console.log(`Example app listening on port 3000`);
});
